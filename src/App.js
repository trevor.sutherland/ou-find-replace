import React, { Component } from 'react';
import axios from 'axios';
import './App.css';
import Checkbox from './Control/Checkbox';
import Replace from './Control/Replace';
import { Form, Button } from 'react-bootstrap';
import RegexRadio from './Control/RegexRadio';
import qs from 'qs';

class App extends Component {
  constructor(props) {
    super(props); 
        this.state = {
          sitesList: [],
          srchStr: "",
          rplcStr: "",
          loading: true,
          findReplaceJobs: [],
          findReplaceResults: [],
          files: [],
          submitted: false,
          checked: false,
          publish: false,
          allChecked: false,
          replaceInFiles: false,
          searchType: ""
        }
  
  this.handleChange = this.handleChange.bind(this);
  this.handleSubmit = this.handleSubmit.bind(this);
  this.handleReplace = this.handleReplace.bind(this);
  this.findReplace = this.findReplace.bind(this);
  this.getSites = this.getSites.bind(this);
  this.handleSrchStr = this.handleSrchStr.bind(this);
  this.handleRplcStr = this.handleRplcStr.bind(this);
  this.jobStatus = this.jobStatus.bind(this);
  this.jobComplete = this.jobComplete.bind(this);
  this.handleBack = this.handleBack.bind(this);
  this.siteListState = this.siteListState.bind(this);
  this.handleSearchType = this.handleSearchType.bind(this);
  this.handleReviewPublish = this.handleReviewPublish.bind(this);
  this.publishFiles = this.publishFiles.bind(this);
  this.getFilestoPublish = this.getFilestoPublish.bind(this);
}

        jobStatus(currentJob) {
          window.gadget.ready().then(window.gadget.fetch).then(
            function () {

              let apihost = window.gadget.apihost
              let token = window.gadget.token

              console.log('Find replace results');
                let config = {
                  method: 'get',
                  url: apihost + '/sites/findreplacestatus',
                  params: {
                    'authorization_token': token,
                    site: currentJob[1],
                    id: currentJob[0]
                    }
                  }
              axios(config)
              .then((response) => {
                  const jobResponse = response.data
                  this.jobComplete(jobResponse, currentJob)
                  console.log(jobResponse)
              })
              
              .catch((error) =>console.log(error));

            }.bind(this));
        }
          
        
        jobComplete (jobResponse, currentJob) {
          if (jobResponse.finished !== false) {
            this.setState({loading: false})
            const completedJob = jobResponse
            const siteObject = { "site": currentJob[1] }
            
            Object.assign(completedJob, siteObject);

            this.setState(
              {
              findReplaceResults: [...this.state.findReplaceResults, jobResponse] 
              },
              console.log(this.state.findReplaceResults)
            )
          }
          else {
            this.jobStatus(currentJob)
          }
        }


        findReplace (site) {
          const currentSite = arguments[0];
          window.gadget.ready().then(window.gadget.fetch).then(
            function () {

              let apihost = window.gadget.apihost
              let token = window.gadget.token

              console.log(apihost);
              console.log(token);
              console.log('Running find replace...');
                let config = {
                  method: 'post',
                  url: apihost + '/sites/findreplace',
                  params: {
                    'authorization_token': token,
                    site: site,
                    path: "/",
                    srchstr: this.state.srchStr,
                    rplcstr: this.state.rplcStr,
                    replace: this.state.replaceInFiles,
                    srchtype: this.state.searchType
                    }
                  }
                axios(config)
                .then((response) => {
                  const currentJob = [response.data.id, currentSite]

                    this.setState(
                      { 
                        findReplaceJobs: [...this.state.findReplaceJobs, currentJob] 
                      }, 
                      this.jobStatus(currentJob)
                      );  
                })
              .catch((error) =>console.log(error)); 
              }.bind(this));
        }
        
        
        
        getFilestoPublish (e) {
          e.preventDefault();
          
          for (const key of this.state.findReplaceResults) {
          console.log(key.site);
          let filePaths = {
            paths: []
          };
            for (const id of key.files) {
              filePaths.paths.push(id.path);
            }
          this.publishFiles(filePaths, key.site)
          console.log(filePaths);
          console.log(filePaths.paths);
          }
        }

        
        publishFiles (files, site) {
          window.gadget.ready().then(window.gadget.fetch).then(
            function () {
              console.log(JSON.stringify(files.paths));

              let parsedFiles = JSON.stringify(files.paths);
              let apihost = window.gadget.apihost
              let token = window.gadget.token
              let data = qs.stringify({
              'target': site,
              'site': site,
              'paths': parsedFiles
              });
                console.log(data);

                console.log('publishing...' + site);
                let config = {
                  method: 'post',
                  url: apihost + '/files/multipublish',
                  headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                  params: {
                    'authorization_token': token,
                    },
                    data: data
                  }
                axios(config)
              .catch((error) =>console.log(error));   
            });
        } 

        getSites ()  {
              axios.get('./site-list-response.json')
              .then((response) => {
                console.log(response.data);
                
                let OUResponse = response.data
                for (const key of OUResponse) {
                  const siteObject = { value: key.site, label: key.site }
                  // let sitesListObject =  Object.assign(siteObject, sitesList);
                  this.siteListState(siteObject);

                }
              })
              .catch((error) =>console.log(error));
            }
            
        siteListState(siteObject) {
            this.setState({
              sitesList: [...this.state.sitesList, siteObject]
            });
            console.log(this.state.sitesList)
        }
 

        handleSubmit(e) {
          e.preventDefault();
          for (const key of this.state.sitesList) {
            if (key.checked !== false) {
            this.findReplace(key.site);
            }
          }
          this.setState({submitted: true});
        }

        handleBack(e) {
          e.preventDefault();
          this.setState({submitted: false});
          this.setState({findReplaceResults: []});
          this.setState({files:[]});
          this.setState({findReplaceJobs: []});
          this.setState({loading: true});
          this.setState({publish: false});
          this.setState({replaceInFiles: false});
        }

        handleReplace() {
          this.setState({replaceInFiles: true})
          this.setState({loading: true});
          this.setState({findReplaceResults: []});
          this.setState({findReplaceJobs: []});
          this.setState({files:[]});
          for (const key of this.state.sitesList) {
            if (key.checked !== false) {
            this.findReplace(key.site);
            }
          }
        }

        handleReviewPublish(e) {
          e.preventDefault();
          this.setState({publish: true})
          this.setState({submitted: false})
        }

        handleSrchStr(e) {
          e.preventDefault();
          this.setState({srchStr: e.target.value});
        }

        handleRplcStr(e) {
          e.preventDefault();
          this.setState({rplcStr: e.target.value});
        }

        handleCheckboxStateChange(value, event) {
          if (event.action === "select-option" && event.option.value === "*") {
            this.setState({selectedOptions: [{label: "All", value: "*"}, ...this.state.options]})
          } else if (
            event.action === "deselect-option" &&
            event.option.value === "*"
          ) {
            this.setState({selectedOptions: []});
          } else if (event.action === "deselect-option") {
            this.setState({selectedOptions: value.filter((o) => o.value !== "*")});
          } else {
            this.setState({selectedOptions: value});
          }
          console.log(this.state.selectedOptions)
          console.log(this.state.selectedOptions);
        }

        handleChange = e => {
          let itemName = e.target.name;
          let checked = e.target.checked;
          this.setState(prevState => { 
            let { sitesList, allChecked } = prevState;
            if (itemName === "checkAll") {
              allChecked = checked;
              sitesList = sitesList.map(item => ({ ...item, checked: checked }));
            } else {
              sitesList = sitesList.map(item =>
                item.site === itemName ? { ...item, checked: checked } : item
              );
              allChecked = sitesList.every(item => item.checked);
            }
            return { sitesList, allChecked };
          });
          }
        handleSearchType (e) {
          this.setState({searchType: e.target.value})
        }

      componentDidMount() {
        this.getSites();
        console.log(this.state.findReplaceJobs)
    }      
  
  render()  {
  return (
    <div className="App">
      <div className='container-fluid'>

      {(this.state.submitted === true) ? // conditional render

      <div className='row'>
      <div className='col-md-12'>

      <><Button variant="primary" type="submit" onClick={this.handleBack}>Back to Form</Button>{'   '}
      <Button variant="primary" type="submit" onClick={this.handleReplace}>Replace</Button></>
      <Replace loading={this.state.loading} rplcStr={this.state.rplcStr} findReplaceResults={this.state.findReplaceResults}/>
      </div>
      </div>

      :  //else
      <div className='row'>
      <div className='col-md-6' style={{zIndex: 2}}>

        <RegexRadio onChange={this.handleSearchType} searchType={this.state.searchType} />
        <Form onSubmit={this.handleSubmit}>
          <Form.Group controlId="Find">
            <Form.Label>Find</Form.Label>
            <Form.Control type="text" placeholder="find string" value={this.state.srchStr} onChange={this.handleSrchStr} />
          </Form.Group>
          <Form.Group controlId="replace">
            <Form.Label>Replace</Form.Label>
            <Form.Control type="text" placeholder="replace string" value={this.state.rplcStr} onChange={this.handleRplcStr} />
          </Form.Group>
          <Button variant="primary" type="submit">Submit</Button>
        </Form>
      </div> 
      <div className='col-md-6'>
        <Form>
          <Form.Label>
            Sites
          </Form.Label>
          <Checkbox sitesList={this.state.sitesList} onChange={this.handleChange} />
        </Form>
      </div>
      </div>
      
      } 

      </div> 
      </div>
      
  );
}
 }
export default App;
