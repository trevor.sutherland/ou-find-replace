import React, { Component } from 'react';
import { Form } from 'react-bootstrap';
import { ListGroup } from 'react-bootstrap';
import {Accordion, Card, Button, Spinner } from 'react-bootstrap';

  class Replace extends Component {
      
      render() {
        if (this.props.loading) {
          return <div><span className='loading'>Finding files... </span><Spinner size="lg" animation="border" role="status" variant="primary">
          <span className="sr-only">Loading...</span>
        </Spinner></div>
        }
        return this.props.findReplaceResults.map((item => 
        <Accordion>
          <Card>
            <Card.Header>
              <Accordion.Toggle as={Button} variant="link" eventKey="0">
                {item.site + " (" + item.status + ")"}
              </Accordion.Toggle>
            </Card.Header>
           <Accordion.Collapse eventKey="0">
              <Card.Body>
                <ListGroup>
                  { item.files.map((files) => 
                  <Form>
                    <Form.Check className="file-replace-name"
                      label={files.path} 
                      type="checkbox" 
                      name={files.path}/>
                    {files.context.map((context) =>
                    <div className="file-context"><p><span>{context.before}</span>
                    <span className="context-match replaced">{context.match}</span>
                    <span className="context-replace">{this.props.rplcStr}</span>
                    <span>{context.after}</span></p></div>
                    )}
                     
                  </Form>
                  )}
                </ListGroup>
              </Card.Body>
            </Accordion.Collapse>
          </Card>
      </Accordion>
    )
);
}
}
export default Replace