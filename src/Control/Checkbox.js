import React, { useState, useEffect } from 'react';
import MultiSelect from 'react-multi-select-component';
import options from './options';

const Checkbox = ({ sitesList }) => {
  console.log(sitesList)
  console.log(typeof sitesList)
  const [selected, setSelected] = useState([]);
  
  // useEffect(() => {
  // const siteListOptions = props.sitesList.map((item) => ({ label: item.site, value: item.site }))
  // console.log(siteListOptions);
  // })
  console.log(selected)
  const customValueRenderer = (selected, _options) => {
    return selected.length
      ? selected.map(({ label }) => "✔️ " + label)
      : "😶 No Items Selected";
  };
    return (
      <div>
        <h3>Select Sites</h3>
        <MultiSelect
          options = {options}
          valueRenderer = {customValueRenderer}
          value={selected}
          onChange={setSelected}
          labelledBy="Select"

        />
      </div>
    )
  }
export default Checkbox;
