import React, { Component } from 'react';
import { Accordion, Button, Card } from 'react-bootstrap';
import checkboxes from '../checkboxes';
  
  class SiteToggle extends Component {
      
      render() {
        
        return ( checkboxes.map((item) => (
            
            <Card.Header>
            <Accordion.Toggle as={Button} variant="link" eventKey="0">
            {item.site}
          </Accordion.Toggle>
          </Card.Header>


        
        ))
        );
      }
  }

export default SiteToggle