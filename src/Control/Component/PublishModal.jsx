import React, { Component } from 'react';
import { Modal, Button } from 'react-bootstrap';
  
  class PublishModal extends Component {
      
      render() {
        const [show, setShow] = false;
        const handleClose = () => setShow(false);
        const handleShow = () => setShow(true);
        return ( 
        <>
          <Button variant="primary" onClick={handleShow}>
            Launch demo modal
          </Button>
    
          <Modal show={show} onHide={handleClose} animation={false}>
            <Modal.Header closeButton>
              <Modal.Title>Modal heading</Modal.Title>
            </Modal.Header>
            <Modal.Body>Woohoo, you're reading this text in a modal!</Modal.Body>
            <Modal.Footer>
              <Button variant="secondary" onClick={handleClose}>
                Close
              </Button>
              <Button variant="primary" onClick={handleClose}>
                Save Changes
              </Button>
            </Modal.Footer>
          </Modal>
        </>
        );
      }
  }

export default PublishModal