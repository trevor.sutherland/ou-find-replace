import React, { Component } from 'react';
import { Card, Accordion, Button } from 'react-bootstrap';
import ListGroup from 'react-bootstrap/ListGroup';
  
  class PublishPath extends Component {
      
    render() {
      return this.props.findReplaceResults.files.map((item => 
          <ListGroup.Item>{item}</ListGroup.Item>
        )
      );
    }
}

export default PublishPath