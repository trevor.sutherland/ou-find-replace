import React, { Component } from 'react';
import Form from 'react-bootstrap/Form'


class SelectAll extends Component {
    render() {
        return (
          <Form.Check 
          type="checkbox"
          name="checkAll"
          label='Select All'
          checked={this.props.allChecked}
          onChange={this.props.onChange}>
          </Form.Check>
        );
      }
    }

export default SelectAll;