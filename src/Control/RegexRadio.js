import React, { Component } from 'react';
import Form from 'react-bootstrap/Form';


class RegexRadio extends Component {
  render() {
    return ( 
    
    <Form>
      <Form.Group>
      <Form.Label as="legend" column sm={2}>
        Type:
      </Form.Label>
        <Form.Check
          inline
          type="radio"
          label="Literal Text"
          name="formHorizontalRadios"
          id="inline-radio-1"
          value="string"
          checked
          onChange={this.props.onChange}
        />
        <Form.Check
          inline
          type="radio"
          label="Regular Expression"
          name="formHorizontalRadios"
          id="inline-radio-2"
          value="regex"
          onChange={this.props.onChange}
        />
    </Form.Group>
    </Form>
    
    )
  }
}

export default RegexRadio;